# Drag and Drop in Unity

So you are looking for information how to implement drag and drop? That's easy! This repository contain what you looking for! There are two scripts that do all of that!

In addition to this repository I also made a post about it that you can find here: https://www.patrykgalach.com/2019/05/09/drag-and-drop-in-unity/

Enjoy!

---

# How to use it?

This repository contains an example of how you can Drag and Drop in Unity.

If you want to see that implementation, go straight to [Assets/Scripts/](https://bitbucket.org/gaello/drag-and-drop/src/master/Assets/Scripts/) folder. You will find all code that I wrote to make it work. Code also have comments so it would make a little bit more sense.

I hope you will enjoy it!

---

#Well done!

You have just learned how to implement Drag and Drop mechanic in Unity!

##Congratulations!

For more visit my blog: https://www.patrykgalach.com
